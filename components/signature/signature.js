// components/autograph/index.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    width: {
      type: String,
      value: '100%'
    },
    height: {
      type: String,
      value: '600rpx'
    },
    lineWidth: {
      type: Number,
      value: 2
    },
    lineColor: {
      type: String,
      value: '#000000'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    isDraw: false, // 是否能画画
    canvasElement: null,
    canvasContext: null,
    oldPosition: {
      x: 0,
      y: 0
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 初始化canvas
    initCanvas(fn) {
      const query = wx.createSelectorQuery().in(this)
      query.select('.autograph-canvas').fields({
        node: true,
        size: true,
        context: true
      }).exec(res => {

        const canvas = res[0].node
        const context = canvas.getContext('2d')
        const dpr = wx.getSystemInfoSync().pixelRatio
        canvas.width = res[0].width * dpr
        canvas.height = res[0].height * dpr
        context.scale(dpr, dpr)
        this.setData({
          canvasElement: canvas,
          canvasContext: context,
        }, function () {
          if (typeof fn == 'function') {
            fn()
          }
        })
      })
    },
    // 描绘canvas
    drawCanvas(position) {
      if (this.data.canvasContext) {

        this.data.canvasContext.strokeStyle = this.properties.lineColor
        this.data.canvasContext.lineWidth = this.properties.lineWidth

        this.data.canvasContext.beginPath()
        this.data.canvasContext.moveTo(this.data.oldPosition.x, this.data.oldPosition.y)
        this.data.canvasContext.lineTo(position.x, position.y)
        this.data.canvasContext.stroke()
        this.data.canvasContext.closePath();

        this.setData({
          oldPosition: {
            x: position.x,
            y: position.y
          }
        })
      }
    },
    // 在画布触摸开始
    handleTouchStart(e) {
      var self = this;
      const x = e.touches[0].x;
      const y = e.touches[0].y;
      self.setData({
        oldPosition: {
          x: x,
          y: y
        },
      }, () => {
        self.setData({
          isDraw: true,
        })
      })


    },
    // 在画布触摸移动
    handleTouchMove(e) {
      if (this.data.isDraw) {
        let positionItem = e.touches[0]
        if (this.data.canvasContext) {
          this.drawCanvas(positionItem, true)
        } else {
          this.initCanvas(() => {
            this.drawCanvas(positionItem, true)
          })
        }
      }
    },
    // 在画布触摸结束
    handleTouchEnd() {
      this.setData({
        isDraw: false
      })
    },
    // 清除画布
    clearCanvas() {
      if(this.data.canvasElement) {
        const x = this.data.canvasElement.width
        const y = this.data.canvasElement.height
        this.data.canvasContext.clearRect(0, 0, x, y)
      }

    },
    // 获取画布的base64
    getCanvasBase64() {
      if(this.data.canvasElement) {
        const pngPic = this.data.canvasElement.toDataURL()
        return pngPic
      }

      return false;

    },
    // 获取临时文件
    getCavasTempFile() {
      return new Promise((resolve, reject) => {
        wx.canvasToTempFilePath({
          x: 0,
          y: 0,
          // width: this.properties.width,
          // height: this.properties.height,

          // destWidth:this.properties.width,
          // destHeight:this.properties.height,

          canvas: this.data.canvasElement,
          success: (res) => {
            console.log("getCavasTempFile res", res);
            resolve(res)
          },
          fail: () => {
            reject('图片临时文件生成失败')
          }
        })
      })
    }
  }
})

// ————————————————
// 版权声明：本文为CSDN博主「歪歪的橘猫」的原创文章，遵循CC 4.0 BY-SA版权协议，转载请附上原文出处链接及本声明。
// 原文链接：https://blog.csdn.net/zzaixiao/article/details/125222920