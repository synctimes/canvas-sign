// pages/demo/demo.js
Page({
  /**
   * 页面的初始数据
   */
  // pages/useAutograph/index.js
  data: {
    signBase64: ''
  },
  // 清除画布
  handleReSign() {
    const autograph = this.selectComponent(".autograph-component")
    autograph.clearCanvas()
  },
  // 获取base64
  handleSure() {
    const autograph = this.selectComponent(".autograph-component")
    this.setData({
      signBase64: autograph.getCanvasBase64()
    })
  },
  // 获取临时文件路径
  getTempFile() {
    const autograph = this.selectComponent(".autograph-component")
    autograph.getCavasTempFile().then(res => {
      this.setData({
        signBase64: res
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})